const timeline = [
  {
    head: "Phoenix",
    text: "Born in the vally of the sun.",
    year: "1993",
    img: "Flag_of_Phoenix.png",
    mask: true
  },
  {
    head: "Saint Louis",
    text: "Moved to where the waffle cone and gooey buttercake was invented.",
    year: "2000",
    img: "Flag_of_Saint_Louis.png",
    mask: true
  },
  {
    head: "Nürnberg",
    text: "Moved to the city that makes the best gingerbread and was home to the inventor of the pocket watch as a part of the Congress Bundestag Youth Exchange (CBYX).",
    year: "2011",
    img: "Nürnberg.png",
    mask: false
  },
  {
    head: "Johannes Scharrer Gymnasium",
    text: "Attended the 13th grade at the JSG.",
    year: "2011",
    img: "JSG.png",
    mask: true
  },
  {
    head: "Memphis",
    text: "Put on my blue suede shoes and boared a plane. Touched down in the land of the Delta Blues... ~ Marc Cohn",
    year: "2012",
    img: "Flag_of_Memphis.png"
  },
  {
    head: "Rhodes College",
    text: "Embarked on my pursuit of higher education.",
    year: "2012",
    img: "Rhodes_Seal_Colored.png",
    mask: true
  },
  {
    head: "Neuroendocrinology",
    text: "Undertook research on agonistic / social behavior with Dr. David Kabelik .",
    year: "2012",
    img: "anole.png",
    mask: true
  },
  {
    head: "Brandeis",
    text: "National Science Foundation research experience for undergraduates with Dr. Paul Miller",
    year: "2014",
    img: "Brandeis_University_seal.png",
    mask: true
  },
  {
    head: "Epidemiology",
    text: "Began modeling Ebola epidemiology with Dr. Erin Bodine.",
    year: "2014",
    img: "ebola.png",
    mask: true
  },
  {
    head: "Stowers Institute for Medical Research",
    text: "Partook in a fellowship with Dr. Kausik Si.",
    year: "2015",
    img: "stowers.png",
    mask: true
  },
  {
    head: "Bachelor of Science",
    text: "Graduated with a bachelor of science in neuroscience, biomathematics, and computer science.",
    year: "2016",
    img: "Rhodes_Seal_Colored.png",
    mask: true
  },
  {
    head: "Göttingen",
    text: "Moved to the city of the Göttingen eight.",
    year: "2016",
    img: "Göttingen.png",
    mask: true
  },
  {
    head: "Deutsches Zentrum für Neurodegenerative Erkrankungen",
    text: "Joined Dr. Stefan Bonn's lab as a statistician.",
    year: "2016",
    img: "dzne-eni.png",
    mask: true
  },
  {
    head: "European Neuroscience Institute",
    text: "Boehringer Ingelheim Fonds Fellow with Dr. Ira Milosevic.",
    year: "2016",
    img: "dzne-eni.png",
    mask: true
  },
  {
    head: "University of Göttingen",
    text: "Began Ph.D. in computational neuroscience.",
    year: "2016",
    img: "UMG_Colored_Seal.svg",
    mask: true
  },
  {
    head: "Hamburg",
    text: "Moved to a port town.",
    year: "2017",
    img: "Flag_of_Hamburg.svg",
    mask: true
  },
  {
    head: "Zentrum für Molekulare Neurobiologie Hamburg",
    text: "Joined the Artificial Intelligence team at the Institute for Molecular Systems Biology",
    year: "2017",
    img: "zmnh.svg",
    mask: false
  },
]

export default timeline
