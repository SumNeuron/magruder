const posters = [
  {
    'title': 'Dual Homeostasis: Preserving Information',
    'authors': [
      {name: 'Daniel Sumner Magruder'},
      {name: 'Paul Miller'}
    ],
    'journal': 'REU',
    'journal abbreviation': undefined,
    'publisher': 'Brandeis University',
    'year': 2014,
    'month': undefined,
    'day': undefined,
    'volume': undefined,
    'issue': undefined,
    'pages': 1.0,
    'page': undefined,
    'pmid': undefined,
    'issn': undefined,
    'pmcid': undefined,
    'doi': undefined,
    'type': 'poster',
    'url': undefined,
    'abstract': undefined,
    'preview': 'dual-homeostasis-preview.png',
    'filename': 'dual-homeostasis.pdf',
    'pdf': undefined
  },
  {
    'title': 'Constraining Conformation',
    'authors': [
      {name: 'Daniel Sumner Magruder'},
      {name: 'Liying Li'},
      {name: 'Jay Unruh'},
      {name: 'Kausik Si'}
    ],
    'journal': 'Summer Scholars',
    'journal abbreviation': undefined,
    'publisher': 'Stowers Institute for Medical Research',
    'year': 2015,
    'month': undefined,
    'day': undefined,
    'volume': undefined,
    'issue': undefined,
    'pages': undefined,
    'page': undefined,
    'pmid': undefined,
    'issn': undefined,
    'pmcid': undefined,
    'doi': undefined,
    'type': 'poster',
    'url': undefined,
    'abstract': undefined,
    'preview': 'constraining-conformation-preview.png',
    'filename':'constraining-conformation.pdf',
    'pdf': undefined
  },
]
 export default posters
