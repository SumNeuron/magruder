const skills = [
{
  'skill': 'Awk',
  'category': 'Programming language',
  'tags': 'text processing, data analysis',
  'started': '6 1 2016',
  'stopped': '7 1 2017',
  'icon': 'code'
},
{
  'skill': 'Bash',
  'category': 'Programming language',
  'tags': '',
  'started': '6 1 2014',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'C++',
  'category': 'Programming language',
  'tags': 'data types, algorithms, computational neuroscience, mathematical modeling',
  'started': '8 1 2012',
  'stopped': '5 1',
  'icon': 'code'
},
{
  'skill': 'LaTeX',
  'category': 'Programming language',
  'tags': 'typesetting',
  'started': '8 1 2012',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'Wolfram Mathematica',
  'category': 'Programming language',
  'tags': 'software development, machine learning, data analysis, bioinformatics, machine learning, mathematical modeling',
  'started': '8 1 2012',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'MatLab',
  'category': 'Programming language',
  'tags': 'mathematical modeling',
  'started': '8 1 2012',
  'stopped': '8 1 2014',
  'icon': 'code'
},
{
  'skill': 'NetLogo',
  'category': 'Programming language',
  'tags': 'mathematical modeling, agent based modeling',
  'started': '8 1 2012',
  'stopped': '8 1 2012',
  'icon': 'code'
},
{
  'skill': 'Objective-C',
  'category': 'Programming language',
  'tags': 'iOS, mobile applications, game development',
  'started': '8 1, 2012',
  'stopped': '5 1 2016',
  'icon': 'code'
},
{
  'skill': 'Python',
  'category': 'Programming language',
  'tags': 'software development, web development, full stack, machine learning, data analysis, bioinformatics',
  'started': '8 1 2012',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'R',
  'category': 'Programming language',
  'tags': 'data analysis, bioinformatics, next-generation genomics',
  'started': '6 1 2016',
  'stopped': '6 1 2017',
  'icon': 'code'
},
{
  'skill': 'Swift',
  'category': 'Programming language',
  'tags': 'iOS, mobile applications, game development',
  'started': '8 1 2012',
  'stopped': '6 1 2017',
  'icon': 'code'
},
{
  'skill': 'JavaScript',
  'category': 'Programming language',
  'tags': 'interactive, web development, full stack, svg, design, data analysis, visualization',
  'started': '6 1 2016',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'Ruby',
  'category': 'Programming language',
  'tags': 'web development, full stack',
  'started': '6 1 2016',
  'stopped': '6 1 2017',
  'icon': 'code'
},
{
  'skill': 'CSS3',
  'category': 'Programming language',
  'tags': 'interactive, design, visualization',
  'started': '6 1 2016',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'HTML5',
  'category': 'Programming language',
  'tags': 'web development, full stack',
  'started': '6 1 2016',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'Docker',
  'category': 'Programming language',
  'tags': 'software development',
  'started': '7 1 2017',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'Git',
  'category': 'Programming language',
  'tags': 'software development',
  'started': '6 1 2016',
  'stopped': undefined,
  'icon': 'code'
},
{
  'skill': 'English',
  'category': 'Language',
  'tags': 'mother tongue',
  'started': '9 24 1993',
  'stopped': undefined,
  'icon': 'translate'
},
{
  'skill': 'German',
  'category': 'Language',
  'tags': '',
  'started': '8 1 2008',
  'stopped': undefined,
  'icon': 'translate'
},
{
  'skill': 'Adobe Illustrator',
  'category': 'Software',
  'tags': 'svg, design, scientific illustration',
  'started': '8 1 2012',
  'stopped': '8 1 2014',
  'icon': 'create'
},
{
  'skill': 'Affinity Designer',
  'category': 'Software',
  'tags': 'svg, design, scientific illustration',
  'started': '8 1 2014',
  'stopped': undefined,
  'icon': 'create'
},
{
  'skill': 'Adobe Photoshop',
  'category': 'Software',
  'tags': 'design',
  'started': '8 1 2012',
  'stopped': '8 1 2014',
  'icon': 'create'
},
{
  'skill': 'Microsoft Office Suite',
  'category': 'Software',
  'tags': 'obvious',
  'started': '1 1',
  'stopped': undefined,
  'icon': 'work'
},
{
  'skill': 'iWork Suite',
  'category': 'Software',
  'tags': 'obvious',
  'started': '1 1',
  'stopped': undefined,
  'icon': 'work'
},
{
  'skill': 'TensorFlow',
  'category': 'Library',
  'tags': 'machine learning, data analysis, software development, artificial intelligence',
  'started': '6 1 2018',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'MXNet',
  'category': 'Library',
  'tags': 'machine learning, data analysis, software development, artificial intelligence',
  'started': '6 1 2016',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'NumPy',
  'category': 'Library',
  'tags': 'data analysis, bioinformatics',
  'started': '6 1 2018',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'Pandas',
  'category': 'Library',
  'tags': 'data analysis, bioinformatics',
  'started': '6 1 2018',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'D3.js',
  'category': 'Library',
  'tags': 'interactive, svg, design, data analysis, visualization',
  'started': '6 1 2017',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'Vue.js',
  'category': 'Library',
  'tags': 'front end, web development, progressive web applications, interactive, svg, design, data analysis, visualization',
  'started': '6 1 2018',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'Bootstrap',
  'category': 'Library',
  'tags': 'full stack, web development, front end, design',
  'started': '6 1 2016',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'Jest',
  'category': 'Library',
  'tags': 'unit testing, software development',
  'started': '6 1 2018',
  'stopped': undefined,
  'icon': 'library_books'
},
{
  'skill': 'Rollup.js',
  'category': 'Library',
  'tags': 'software development',
  'started': '6 1 2016',
  'stopped': undefined,
  'icon': 'library_books'
},
]
export default skills
