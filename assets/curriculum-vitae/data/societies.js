const societies = [
  {abbr: 'ΒΒΒ', href: 'https://www.tribeta.org/'},
  {abbr: '∆ΕΙ', href: 'https://sites.google.com/site/deltaepsiloniotaorg/membership'},
  {abbr: 'NPΨ', href: 'https://nurhopsi.org/'},
  {abbr: 'Ο∆Κ', href: 'https://odk.org/'},
  {abbr: 'ΠΣΑ', href: 'https://www.pisigmaalpha.org'},
]

export default societies
