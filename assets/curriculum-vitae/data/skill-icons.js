const icons = [
  {
    name: 'Adobe',
    color: '#FF0000',
  },
  {
    name: 'NPM',
    color: '#CB3837',
  },
  {
    name: 'Rails',
    color: '#CC0000',
  },
  {
    name: 'Adobe Creative Cloud',
    color: '#D41818',
  },
  {
    name: 'Ruby',
    color: '#CC342D',
  },

  {
    name: 'rollup.js',
    color: '#EC4A3F',
  },

  {
    name: 'Wolfram',
    color: '#DD1100',
  },
  {
    name: 'Wolfram Language',
    color: '#DD1100',
  },
  {
    name: 'Wolfram Mathematica',
    color: '#DD1100',
  },


  {
    name: 'Git',
    color: '#F05032',
  },
  {
    name: 'Microsoft PowerPoint',
    color: '#D24726',
  },
  {
    name: 'HTML5',
    color: '#E34F26',
  },
  {
    name: 'Swift',
    color: '#FA7343',
  },
  {
    name: 'Juypter',
    color: '#F37626',
  },
  {
    name: 'Adobe Illustrator',
    color: '#FF7C00',
  },




  {
    name: 'D3.js',
    color: '#F9A03C',
  },

  {
    name: 'JavaScript',
    color: '#F7DF1E',
  },




  {
    name: 'Microsoft Excel',
    color: '#217346',
  },
  {
    name: 'Vue.js',
    color: '#4FC08D',
  },
  {
    name: 'Adobe Photoshop',
    color: '#00C8FF',
  },
  {
    name: 'Docker',
    color: '#1488C6',
  },
  {
    name: 'CSS3',
    color: '#1572B6',
  },
  {
    name: 'C++',
    color: '#00599C',
  },
  {
    name: 'MySQL',
    color: '#4479A1',
  },
  {
    name: 'Python',
    color: '#3776AB',
  },
  {
    name: 'R',
    color: '#276DC3',
  },
  {
    name: 'Bootstrap',
    color: '#563D7C',
  },
  {
    name: 'Django',
    color: '#09E20',
  },
  {
    name: 'Flask',
    color: '#000000',
  },

]
