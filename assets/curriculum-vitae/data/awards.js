const awards = [
  {
    title: 'Barry Goldwater Fellowship Honorable Mention',
    provider: 'Excellence in Education Foundation',
    purpose: 'Honorable Mention in national grant. Proposed a comprehensive analysis of behavioral networks in Anolis sagrei utilizing the data collected in conjunction with Kabelik\'s lab, via PCA, SEM and force fitting to find significant sub-circuits crucial for initiating agonistic, sexual, or null behavior.',
    year: 2014,
    month: undefined,
    institute: 'Rhodes College'
  },
  {
    title: 'Carlisle Award',
    provider: 'Tennessee Intercollegiate State Legislatur',
    purpose: 'Awarded Tennessee Intercollegiate State Legislature\'s (TISL) most prestigious Carlisle Award for eloquence and excellence as a senator. Reinstated and presided as statewide chairman of the Small College Caucus coordinating legislative interests utilizing number of colleges over allotted representatives.',
    year: 2013,
    month: undefined,
    institute: 'Rhodes College'
  },
  {
    title: 'CBYX',
    provider: 'Congress Bundestag Youth Exchange',
    purpose: 'Served as a youth ambassador in Germany following selection in highly competitive national fellowship.',
    year: 2011,
    month: undefined,
    institute: 'Ladue Horton Watkins High School'
  },
  {
    title: 'Eagle Scout',
    provider: 'Boy Scouts of America',
    purpose: 'Obtained highest rank in scouting. Eagle project: the construction of tiled chess tables for elementary schools.',
    year: 2011,
    month: undefined,
    institute: 'Ladue Horton Watkins High School'
  },
]

export default awards
