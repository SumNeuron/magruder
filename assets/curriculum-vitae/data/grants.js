const grants = [
  // {
  //   title: '',
  //   provider: 'Lambda Labs',
  //   purpose: '',
  //   year: 2019,
  //   month: 1,
  //   institute: 'Zentrum für Molekulare Neurobiologie Hamburg'
  // },
  {
    title: 'Boehringer Ingelheim Fonds Travel Grant',
    provider: 'Boehringer Ingelheim Fonds',
    purpose: 'Analyzing Next Generation Sequence data of synaptojanin-1 and endophilin with Dr. Ira Milosevic to find common pathways with neurodegeneration.',
    year: 2016,
    month: 6,
    institute: 'European Neuroscience Institute'
  },
  // {
  //   title: 'GPU Seed Grant',
  //   provider: 'NVIDIA',
  //   purpose: 'Received Titan X to accelerate and support deep learning of an agglomerate of genomic (mRNA, snRNA, SNPs, etc), proteomic (NMR, structural data, etc), tissue-specific data.',
  //   year: 2016,
  //   month: 8,
  //   institute: 'Duetsche Zentrum für Neurodegenerative Erkrankungen'
  // }
]

export default grants
